<?php

/**
 * @file
 * Provides an additional config form for theme settings.
 */

use Drupal\Core\Form\FormStateInterface;

function yg_verb_form_system_theme_settings_alter(array &$form, FormStateInterface $form_state) {
  $form['visibility'] = [
    '#type' => 'vertical_tabs',
    '#title' => t('YG Verb Settings'),
    '#weight' => -999,
  ];

  $form['social']= [
    '#type' => 'details',
    '#title' => t('Social Links'),
    '#weight' => -999,
    '#group' => 'visibility',
    '#open' => FALSE,
  ];
#social links    
  $form['social']['social_links'] = [
    '#type' => 'details',
    '#title' => t('Social Links'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  ];
  $form['social']['social_links']['facebook_url'] = [
    '#type' => 'textfield',
    '#title' => t('Facebook'),
    '#description' => t('Please enter your facebook url'),
    '#default_value' => theme_get_setting('facebook_url'),
  ];
  $form['social']['social_links']['twitter_url'] = [
    '#type' => 'textfield',
    '#title' => t('Twitter'),
    '#description' => t('Please enter your twitter url'),
    '#default_value' => theme_get_setting('twitter_url'),
  ]; 
  $form['social']['social_links']['linkedin_url'] = [
    '#type' => 'textfield',
    '#title' => t('Linkedin url'),
    '#description' => t('Please enter your linkedin url'),
    '#default_value' => theme_get_setting('linkedin_url'),
  ];
  $form['social']['social_links']['dribbble_url'] = [
    '#type' => 'textfield',
    '#title' => t('Dribbble'),
    '#description' => t('Please enter your dribbble url'),
    '#default_value' => theme_get_setting('dribbble_url'),
  ];
  $form['banner'] = [
    '#type' => 'details',
    '#title' => t('Background Image '),
    '#weight' => 12,
    '#group' => 'visibility',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  ];
  $form['banner']['bg-image'] = [
    '#type' => 'managed_file',
    '#title'    => t('Background Image'),
    '#default_value' => theme_get_setting('bg-image'),
    '#upload_location' => 'public://',
    '#description' => t('Choose your background image'),
  ];
  $form['#submit'][] = 'yg_verb_form_submit';

}
 
function yg_verb_form_submit(&$form, $form_state) {
  $fid = $form_state->getValue('bg-image');
  if (array_key_exists(0,$fid)){
    $file = file_load($fid[0]);
    if (!empty($file)) {
      $file->setPermanent();
      $file->save();
      $file_usage = \Drupal::service('file.usage');
      $file_usage->add($file, 'yg_verb', 'themes', 1);
    }
  }
}